#### Implementación de HL7-FHIR MAIS para la comunicación entre prestadores y financieras.

* Java API for HL7 FHIR **HAPI FHIR DSTU2**
* Endpoints REST **JBoss RESTEasy**
* Autenticación oAuth2 **Apache Oltu** 
* Contexts and Dependency Injection(CDI) y storage **JBoss Weld**
* Schema validator **Apache Phloc Schematron**
* Logging **SLF4J**
* [MAIS resources](https://github.com/mpielvitori/mais-fhir-resources-dstu2)